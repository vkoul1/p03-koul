//
//  GameView.h
//  DoodleJumpGame
//
//  Created by Vikas on 19/02/17.
//  Copyright © 2017 Q Corp. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "Jumper.h"
#import "Brick.h"

@interface GameView : UIView {
    
}
@property (nonatomic, strong) Jumper *jumper;
@property (nonatomic, strong) NSMutableArray *bricks;
@property (nonatomic) float tilt;
-(void)arrange:(CADisplayLink *)sender;

@end

