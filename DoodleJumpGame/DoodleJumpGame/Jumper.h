//
//  Jumper.h
//  DoodleJumpGame
//
//  Created by Vikas on 19/02/17.
//  Copyright © 2017 Q Corp. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface Jumper : UIView
@property (nonatomic) float dx, dy;  // Velocity
@end
