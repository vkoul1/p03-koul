//
//  ViewController.h
//  DoodleJumpGame
//
//  Created by Vikas on 19/02/17.
//  Copyright © 2017 Q Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameView.h"

@interface ViewController : UIViewController
@property (nonatomic, strong) IBOutlet GameView *gameView;


@end
