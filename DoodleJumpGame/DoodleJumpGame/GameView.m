//
//  GameView.m
//  DoodleJumpGame
//
//  Created by Vikas on 19/02/17.
//  Copyright © 2017 Q Corp. All rights reserved.
//

#import "GameView.h"

@implementation GameView
@synthesize jumper, bricks;
@synthesize tilt;

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        CGRect bounds = [self bounds];
        
        jumper = [[Jumper alloc] initWithFrame:CGRectMake(bounds.size.width/2, bounds.size.height - 20, 40, 40)];
      //  [jumper setBackgroundColor:[UIColor lightGrayColor]];
        [jumper setDx:0];
        [jumper setDy:10];
        
        UIImageView * imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
        imgView.image = [UIImage imageNamed:@"funny.png"];
        [jumper addSubview:imgView];
        [self addSubview:jumper];
        [self makeBricks:nil];
    }
    return self;
}

-(IBAction)makeBricks:(id)sender
{
    CGRect bounds = [self bounds];
    float width = bounds.size.width * .2;
    float height = 40;
    
    if (bricks)
    {
        for (Brick *brick in bricks)
        {
            [brick removeFromSuperview];
        }
    }
    
    bricks = [[NSMutableArray alloc] init];
    for (int i = 0; i < 13; i++)
    {
       // Brick *b = [[Brick alloc] initWithFrame:CGRectMake(0, 0, width, height)];
       // [b setBackgroundColor:[UIColor blackColor]];
        UIImageView * cloudView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
        cloudView.image = [UIImage imageNamed:@"clouds.png"];
        [self addSubview:cloudView];
        [cloudView setCenter:CGPointMake(rand() % (int)(bounds.size.width * .75), rand() % (int)(bounds.size.height * .95))];
        [bricks addObject:cloudView];
    }
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */
-(void)arrange:(CADisplayLink *)sender
{
    CGRect bounds = [self bounds];
   // NSLog(@"bounds vikas x: %f y: %f && height: %f width: %f", bounds.origin.x,bounds.origin.y,bounds.size.height,bounds.size.width);
    // Apply gravity to the acceleration of the jumper
    [jumper setDy:[jumper dy] - .3];
    
    // Apply the tilt.  Limit maximum tilt to + or - 5
    [jumper setDx:[jumper dx] + tilt];
    if ([jumper dx] > 5)
        [jumper setDx:5];
    if ([jumper dx] < -5)
        [jumper setDx:-5];
    
    // Jumper moves in the direction of gravity
    CGPoint p = [jumper center];
    p.x += [jumper dx];
    p.y -= [jumper dy];
    //NSLog(@"jumper loc vikas x: %f y: %f",p.x,p.y);
    // If the jumper has fallen below the bottom of the screen,
    // add a positive velocity to move him
    if (p.y > bounds.size.height)
    {
        NSLog(@"Game Over\n");
        [jumper setDy:0];
        p.y = bounds.size.height;
        [self gameOver];
    }
    // If we've gone past the top of the screen, wrap around
    if (p.y < 0){
       // NSLog(@"Vikas new set of broicks to be loaded as max height reached");
        [self makeBricks:nil];
        p.y += bounds.size.height;
    }
    // If we have gone too far left, or too far right, wrap around
    if (p.x < 0)
        p.x += bounds.size.width;
    if (p.x > bounds.size.width)
        p.x -= bounds.size.width;
    
    // If we are moving down, and we touch a brick, we get
    // a jump to push us up.
    if ([jumper dy] < 0)
    {
        for (Brick *brick in bricks)
        {
            CGRect b = [brick frame];
            if (CGRectContainsPoint(b, p))
            {
                // Yay!  Bounce!
                NSLog(@"Bounce!");
                [jumper setDy:10];
            }
        }
    }
    
    [jumper setCenter:p];
    // NSLog(@"Timestamp %f", ts);
}

-(void)gameOver{
    //Removing jumper and bricks from the view
    [jumper removeFromSuperview];
    if (bricks)
    {
        for (Brick *brick in bricks)
        {
            [brick removeFromSuperview];
        }
    }
    UILabel *gameOver = [[UILabel alloc] init];
    [gameOver setText:@"Game Over...Well played"];
    [gameOver setTextColor:[UIColor blackColor]] ;
    [gameOver setFont:[UIFont fontWithName:@"TimesNewRomanPS-ItalicMT" size:32.0f]];
    [gameOver sizeToFit];
    [gameOver setCenter: self.center];
    [self addSubview: gameOver];
}
@end
