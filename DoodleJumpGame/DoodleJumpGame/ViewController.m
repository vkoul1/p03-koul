//
//  ViewController.m
//  DoodleJumpGame
//
//  Created by Vikas on 19/02/17.
//  Copyright © 2017 Q Corp. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (nonatomic, strong) CADisplayLink *displayLink;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    _displayLink = [CADisplayLink displayLinkWithTarget:_gameView selector:@selector(arrange:)];
    [_displayLink setPreferredFramesPerSecond:30];
    [_displayLink addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)speedChange:(id)sender
{
    UISlider *s = (UISlider *)sender;
    // NSLog(@"tilt %f", (float)[s value]);
    [_gameView setTilt:(float)[s value]];
}

/*
 Setting the left and right tilts
 */

-(IBAction)buttonPressed:(id)sender
{
    UIButton *selectedButton = (UIButton *)sender;
    NSLog(@"Selected button tag is %ld", (long)selectedButton.tag);
    if((long)selectedButton.tag == 0){
        [_gameView setTilt:-5];
    }
    else{
        [_gameView setTilt:5];
    }
}

@end
